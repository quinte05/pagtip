const express = require('express');
const { dirname } = require('path');
const app = express();
//path une directorios independiente win o linux
const path = require ('path');
//const ejsLint = require('ejs-lint');

//settings
app.set('port', 5000);

app.set('views', path.join(__dirname, '/src/views'));
console.log(__dirname, '/src/views')
app.engine('html', require('ejs').renderFile);
    // ejs agrega logica a html
app.set('view engine', 'ejs');

//middlewares

//routes
app.use(require('./src/routes/index'));

//static files
app.use(express.static(path.join(__dirname, 'src/public')))

//listening server
app.listen(app.get('port'), () =>{
    console.log('server on port', app.get('port'))
}) 
console.log('pagina tp');